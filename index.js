const Cache = require( "node-cache" );

module.exports = function( fn, options ) {
	const cache = new Cache( options );

	const getValue = async ( args, key ) => {
		var value, key = getKey( args, key );

		if (key) {
			value = cache.get( key );
		}

		if (!value) {
			value = await fn( args );

			if (key) {
				cache.set( key, value );
			}
		}

		return value;
	};
	
	getValue.cache = cache;
	
	return getValue;
};

module.exports.lazy = function( fn, options ) {
	const getValue = module.exports( fn, options );
	const { cache } = getValue;
	
	return ( args, key ) => {
		var value, key = getKey( args, key );
		
		if (key) {
			value = cache.get( key );
			if (value) return value;
		}
		
		getValue( args, key );
	};
};

function getKey( args, key ) {
	if (!key) {
		switch( typeof args ) {
			case "undefined":
			case "object":
				key = JSON.stringify( args || null );
				break;
			case "string": 
			case "number":
				key = `${ args }`;
		}
	}

	return key;
}